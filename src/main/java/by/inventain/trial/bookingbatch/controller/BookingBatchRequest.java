package by.inventain.trial.bookingbatch.controller;

import com.fasterxml.jackson.annotation.JsonProperty;

import java.time.LocalDateTime;
import java.time.LocalTime;
import java.util.List;

/**
 * Batch with booking records.
 * </br>
 * It is used as request in RESful service
 *
 * @author alexander.guzanov
 */
public class BookingBatchRequest {
    /**
     * working day starting
     */
    @JsonProperty(required = true)
    private LocalTime beginsAt;
    /**
     * working day finishing
     */
    @JsonProperty(required = true)
    private LocalTime finishesAt;
    /**
     * Scheduling records
     */
    @JsonProperty(required = true)
    private List<BookingRecord> records;

    public LocalTime getBeginsAt() {
        return beginsAt;
    }

    public LocalTime getFinishesAt() {
        return finishesAt;
    }

    public List<BookingRecord> getRecords() {
        return records;
    }

    /**
     * Scheduling record in batch request
     */
    public static class BookingRecord {
        /**
         * scheduling timestamp
         */
        @JsonProperty(required = true, value = "ts")
        private LocalDateTime bookingTimestamp;
        /**
         * Employee who schedules meeting
         */
        @JsonProperty(required = true)
        private String employee;
        @JsonProperty(required = true)
        /**
         * Beginning of meeting
         */
        private LocalDateTime startsAt;
        @JsonProperty(required = true)
        /**
         * Duration of meeting
         */
        private Integer duration;

        public LocalDateTime getBookingTimestamp() {
            return bookingTimestamp;
        }

        public String getEmployee() {
            return employee;
        }

        public LocalDateTime getStartAt() {
            return startsAt;
        }

        public Integer getDuration() {
            return duration;
        }

        @Override
        public String toString() {
            return "BookingRecord{" +
                    "bookingTimestamp=" + bookingTimestamp +
                    ", employee='" + employee + '\'' +
                    ", startsAt=" + startsAt +
                    ", duration=" + duration +
                    '}';
        }
    }
}
