package by.inventain.trial.bookingbatch;

import java.time.format.DateTimeFormatter;

/**
 * @author alexander.guzanov
 */
public interface Constants {
    String IMPORT_BOOKING_BATCH = "IMPORT_BOOKING_BATCH";
    String TIME_FORMAT = "HH:mm";
    String DATE_TIME_FORMAT = "yyyy-MM-dd HH:mm:ss";
    String DATE_FORMAT = "yyyy-MM-dd";

    DateTimeFormatter DATE_TIME_FORMATTER = DateTimeFormatter.ofPattern(DATE_TIME_FORMAT);
    DateTimeFormatter DATE_FORMATTER = DateTimeFormatter.ofPattern(DATE_FORMAT);
    DateTimeFormatter TIME_FORMATTER = DateTimeFormatter.ofPattern(TIME_FORMAT);
}
