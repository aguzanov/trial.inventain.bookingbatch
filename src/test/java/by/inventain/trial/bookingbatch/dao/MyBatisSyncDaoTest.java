package by.inventain.trial.bookingbatch.dao;


import by.inventain.trial.bookingbatch.dao.impl.MyBatisSyncDao;
import com.github.springtestdbunit.DbUnitTestExecutionListener;
import org.junit.Assert;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.dao.CannotAcquireLockException;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.test.context.TestExecutionListeners;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.context.support.DependencyInjectionTestExecutionListener;
import org.springframework.test.context.transaction.TransactionalTestExecutionListener;
import org.springframework.transaction.PlatformTransactionManager;
import org.springframework.transaction.TransactionDefinition;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.transaction.support.DefaultTransactionDefinition;
import org.springframework.transaction.support.TransactionTemplate;

import javax.sql.DataSource;

import static by.inventain.trial.bookingbatch.Constants.IMPORT_BOOKING_BATCH;

/**
 * @author alexander.guzanov
 */
@RunWith(SpringRunner.class)
@SpringBootTest
@TestExecutionListeners({DependencyInjectionTestExecutionListener.class, DbUnitTestExecutionListener.class, TransactionalTestExecutionListener.class})
public class MyBatisSyncDaoTest {
    @Autowired
    private MyBatisSyncDao sync;
    @Autowired
    private PlatformTransactionManager transactionManager;
    @Autowired
    private DataSource ds;


    @Test
    public void testLock() {
        final LockEntity lock = sync.lock(IMPORT_BOOKING_BATCH);
        Assert.assertEquals(IMPORT_BOOKING_BATCH, lock.getLockName());
    }

    @Test(expected = CannotAcquireLockException.class)
    @Transactional
    public void testConcurrentLockDenied() {
        final LockEntity lock = sync.lock(IMPORT_BOOKING_BATCH);
        Assert.assertEquals(IMPORT_BOOKING_BATCH, lock.getLockName());

        new TransactionTemplate(transactionManager, new DefaultTransactionDefinition(TransactionDefinition.PROPAGATION_REQUIRES_NEW)).execute((status) -> {
            final JdbcTemplate jdbcTemplate = new JdbcTemplate(ds);
            jdbcTemplate.execute("SET LOCK_TIMEOUT 10");
            return jdbcTemplate
                    .queryForObject("SELECT lock_name FROM SYNC_PROCESSES WHERE lock_name = ? FOR UPDATE;",
                            new Object[]{IMPORT_BOOKING_BATCH}, String.class);
        });
    }
}
