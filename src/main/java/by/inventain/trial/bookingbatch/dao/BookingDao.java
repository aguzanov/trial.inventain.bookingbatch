package by.inventain.trial.bookingbatch.dao;

import javax.validation.constraints.Null;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.Collection;

/**
 * Schedule entity DAO
 *
 * @author alexander.guzanov
 */
public interface BookingDao {

    /**
     * Store entity
     *
     * @param entity entity
     */
    void insert(BookingEntity entity);

    /**
     * Check if this entity has intersections
     *
     * @param employee   employee
     * @param beginsAt   range beginging
     * @param finishesAt range finishing
     * @return true if we has intersections in databse
     */
    boolean hasIntersection(String employee, LocalDateTime beginsAt, LocalDateTime finishesAt);

    /**
     * Find scheduling records in database
     *
     * @param emploee employee name
     * @param from    range starting
     * @param to      range finiging
     * @return collection of sheduling records, that are ordered by begining date, ending date and employee name
     */
    Collection<BookingEntity> findBy(@Null String emploee, @Null LocalDate from, @Null LocalDate to);
}
