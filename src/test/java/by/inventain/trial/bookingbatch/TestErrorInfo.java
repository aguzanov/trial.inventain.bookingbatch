package by.inventain.trial.bookingbatch;

import org.junit.Assert;
import org.junit.Test;

import static by.inventain.trial.bookingbatch.Utils.toLocalDateTime;
import static by.inventain.trial.bookingbatch.Utils.toLocalTime;

/**
 * @author alexander.guzanov
 */
public class TestErrorInfo {

    @Test
    public void testErrorInfo() {
        final ErrorInfo error = ErrorInfo.error("POST:/schedule");
        Assert.assertEquals(100, error.getErrorCode());
        Assert.assertEquals("Internal server error", error.getDescription());
        Assert.assertEquals(1, error.getDetails().size());
        Assert.assertEquals("POST:/schedule", error.getDetails().get("url"));


        final ErrorInfo wrongRequest = ErrorInfo.wrongRequest("POST:/schedule", "from", "10101");
        Assert.assertEquals(104, wrongRequest.getErrorCode());
        Assert.assertEquals("Wrong request", wrongRequest.getDescription());
        Assert.assertEquals(3, wrongRequest.getDetails().size());
        Assert.assertEquals("POST:/schedule", wrongRequest.getDetails().get("url"));
        Assert.assertEquals("10101", wrongRequest.getDetails().get("value"));
        Assert.assertEquals("from", wrongRequest.getDetails().get("field"));


        final ErrorInfo bookingTimeBeyondWorkingHours =
                ErrorInfo.bookingTimeBeyondWorkingHours(toLocalTime("10:10"),
                        toLocalTime("12:30"),
                        toLocalTime("11:00"),
                        toLocalTime("19:00"));
        Assert.assertEquals(101, bookingTimeBeyondWorkingHours.getErrorCode());
        Assert.assertEquals("Booking time is beyond working hours", bookingTimeBeyondWorkingHours.getDescription());
        Assert.assertEquals(4, bookingTimeBeyondWorkingHours.getDetails().size());
        Assert.assertEquals("10:10", bookingTimeBeyondWorkingHours.getDetails().get("bookingBeginsAt"));
        Assert.assertEquals("12:30", bookingTimeBeyondWorkingHours.getDetails().get("bookingFinishesAt"));
        Assert.assertEquals("11:00", bookingTimeBeyondWorkingHours.getDetails().get("workingDayBeginsAt"));
        Assert.assertEquals("19:00", bookingTimeBeyondWorkingHours.getDetails().get("workingDayFinishesAt"));


        final ErrorInfo duplicateSchedulingTimestamp =
                ErrorInfo.duplicateSchedulingTimestamp(toLocalDateTime("2001-01-01 10:10:00"));
        Assert.assertEquals(103, duplicateSchedulingTimestamp.getErrorCode());
        Assert.assertEquals("Bookings timestamp has duplicates", duplicateSchedulingTimestamp.getDescription());
        Assert.assertEquals(1, duplicateSchedulingTimestamp.getDetails().size());
        Assert.assertEquals("2001-01-01 10:10:00", duplicateSchedulingTimestamp.getDetails().get("ts"));


        final ErrorInfo hasIntersection =
                ErrorInfo.hasIntersection("Alex",
                        toLocalDateTime("2001-01-01 10:10:00"),
                        toLocalDateTime("2001-01-01 20:10:00"));
        Assert.assertEquals(102, hasIntersection.getErrorCode());
        Assert.assertEquals("Booking time intersects with another time", hasIntersection.getDescription());
        Assert.assertEquals(3, hasIntersection.getDetails().size());
        Assert.assertEquals("Alex", hasIntersection.getDetails().get("employee"));
        Assert.assertEquals("2001-01-01 10:10:00", hasIntersection.getDetails().get("bookingBeginsAt"));
        Assert.assertEquals("2001-01-01 20:10:00", hasIntersection.getDetails().get("bookingFinishesAt"));
    }
}
