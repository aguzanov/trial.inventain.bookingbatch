package by.inventain.trial.bookingbatch.controller;

import by.inventain.trial.bookingbatch.BookingException;
import by.inventain.trial.bookingbatch.service.BookingService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.web.bind.annotation.*;

import javax.websocket.server.PathParam;
import java.time.LocalDate;

import static by.inventain.trial.bookingbatch.Constants.DATE_FORMAT;

/**
 * REST service implementation
 *
 * @author alexander.guzanov
 */
@RestController
@RequestMapping("booking/")
public class BookingController {

    @Autowired
    private BookingService service;


    /**
     * Import data and returns schedule for just imported records
     *
     * @param batch request
     * @return schedule table
     * @throws BookingException
     */
    @PostMapping("/import")
    public ScheduleTableResponse importData(final @RequestBody BookingBatchRequest batch) throws BookingException {
        return service.importData(batch);
    }


    /**
     * Query schedule table for some period of time
     *
     * @param from starting day
     * @param to   finishing day
     * @return schedule
     */
    @GetMapping("/schedule")
    @ResponseBody
    public ScheduleTableResponse getSchedule(@RequestParam(name = "from", required = false) @DateTimeFormat(pattern = DATE_FORMAT) final LocalDate from,
                                             @RequestParam(name = "to", required = false) @DateTimeFormat(pattern = DATE_FORMAT) final LocalDate to) {

        return service.getSchedule(null, from, to);
    }

    /**
     * Query schedule table for some period of time for employee
     *
     * @param from starting day
     * @param to   finishing day
     * @return schedule
     */
    @GetMapping("/schedule/{employee}")
    @ResponseBody
    public ScheduleTableResponse getSchedule(@PathParam("employee") final String employee,
                                             @RequestParam(name = "from", required = false) @DateTimeFormat(pattern = DATE_FORMAT) final LocalDate from,
                                             @RequestParam(name = "to", required = false) @DateTimeFormat(pattern = DATE_FORMAT) final LocalDate to) {
        return service.getSchedule(employee, from, to);
    }
}
