package by.inventain.trial.bookingbatch.dao.impl;

import by.inventain.trial.bookingbatch.dao.LockEntity;
import by.inventain.trial.bookingbatch.dao.SyncDao;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

/**
 * MyBatis {@link SyncDao}ao implementation
 *
 * @author alexander.guzanov
 */
@Mapper
public interface MyBatisSyncDao extends SyncDao {
    LockEntity lock(@Param("lockName") String lockName);
}
