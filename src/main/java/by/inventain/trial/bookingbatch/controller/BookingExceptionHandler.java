package by.inventain.trial.bookingbatch.controller;

import by.inventain.trial.bookingbatch.BookingException;
import by.inventain.trial.bookingbatch.ErrorInfo;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.exc.InvalidFormatException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.http.converter.HttpMessageNotReadableException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.method.annotation.MethodArgumentTypeMismatchException;

import javax.servlet.http.HttpServletRequest;
import java.util.List;

/**
 * Error handler processor.
 * <p>
 * Error response return status 400 and contents:
 * <pre>
 * {@code
 * {
 *  "errorCode": 103,
 *  "description": "Bookings timestamp has duplicates",
 *  "details": {
 *      "ts": "2001-01-01 01:03:01"
 *   }
 * }
 * }
 * </pre>
 *
 * @author alexander.guzanov
 */

@ControllerAdvice
public class BookingExceptionHandler {
    private static final Logger logger = LoggerFactory.getLogger(BookingExceptionHandler.class);

    @ResponseStatus(HttpStatus.BAD_REQUEST)
    @ExceptionHandler(BookingException.class)
    @ResponseBody
    public ErrorInfo handleBadRequest(HttpServletRequest req, BookingException ex) {
        return ex.getErrorInfo();
    }

    @ResponseStatus(HttpStatus.BAD_REQUEST)
    @ExceptionHandler(Exception.class)
    @ResponseBody
    public ErrorInfo handleBadRequest(HttpServletRequest req, Exception ex) {
        logger.error("Error invoking " + getUrl(req), ex);
        if (ex instanceof MethodArgumentTypeMismatchException) {
            final MethodArgumentTypeMismatchException e = (MethodArgumentTypeMismatchException) ex;
            return ErrorInfo.wrongRequest(getUrl(req), e.getName(), e.getValue());
        } else if (ex instanceof HttpMessageNotReadableException) {
            HttpMessageNotReadableException e = (HttpMessageNotReadableException) ex;
            Object value = null;
            String reference = null;
            final Throwable cause = e.getCause();
            if (cause instanceof InvalidFormatException) {
                InvalidFormatException causeEx = (InvalidFormatException) cause;
                value = causeEx.getValue();
                final List<JsonMappingException.Reference> refs = causeEx.getPath();
                if (refs.size() > 0) {
                    reference = refs.get(refs.size() - 1).getFieldName();
                }
            }
            return ErrorInfo.wrongRequest(getUrl(req), reference, value);
        } else {
            return ErrorInfo.error(getUrl(req));
        }
    }

    private String getUrl(HttpServletRequest req) {
        return req.getMethod() + ":" + req.getRequestURI();
    }
}
