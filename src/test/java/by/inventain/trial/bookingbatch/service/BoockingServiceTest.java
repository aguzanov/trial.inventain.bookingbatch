package by.inventain.trial.bookingbatch.service;

import by.inventain.trial.bookingbatch.BookingException;
import by.inventain.trial.bookingbatch.Constants;
import by.inventain.trial.bookingbatch.controller.BookingBatchRequest;
import by.inventain.trial.bookingbatch.controller.ScheduleTableResponse;
import by.inventain.trial.bookingbatch.dao.BookingDao;
import by.inventain.trial.bookingbatch.dao.BookingEntity;
import by.inventain.trial.bookingbatch.dao.SyncDao;
import mockit.*;
import org.junit.Assert;
import org.junit.Test;
import org.springframework.dao.DuplicateKeyException;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

import static by.inventain.trial.bookingbatch.Utils.toLocalDate;
import static by.inventain.trial.bookingbatch.Utils.toLocalDateTime;
import static by.inventain.trial.bookingbatch.Utils.toLocalTime;

/**
 * @author alexander.guzanov
 */
public class BoockingServiceTest {
    @Tested
    private BookingService service;
    @Injectable
    private BookingDao booking;
    @Injectable
    private SyncDao sync;

    @Test
    public void testBookingTimeBeyondWorkingDay_1(@Mocked final BookingBatchRequest batch, @Mocked BookingBatchRequest.BookingRecord record) {
        new Expectations(){{
            batch.getBeginsAt();result = toLocalTime("09:00");
            batch.getFinishesAt();result = toLocalTime("18:00");
            batch.getRecords();result = Collections.singletonList(record);
            record.getStartAt(); result = toLocalDateTime("2001-01-02 08:00:00");
            record.getDuration(); result = 2;
        }};
        try {
            service.importData(batch);
            Assert.fail("Expected exceptin here!");
        }catch (BookingException e){
            Assert.assertEquals(101, e.getErrorInfo().getErrorCode());
        }

        new Verifications(){{
            sync.lock(Constants.IMPORT_BOOKING_BATCH); times = 1;
            booking.hasIntersection(anyString,(LocalDateTime)any,(LocalDateTime)any); times = 0;
            booking.insert((BookingEntity) any); times = 0;
        }};
    }

    @Test
    public void testBookingTimeBeyondWorkingDay_2(@Mocked final BookingBatchRequest batch, @Mocked BookingBatchRequest.BookingRecord record) {
        new Expectations(){{
            batch.getBeginsAt();result = toLocalTime("09:00");
            batch.getFinishesAt();result = toLocalTime("18:00");
            batch.getRecords();result = Collections.singletonList(record);
            record.getDuration(); result = 9;
            record.getStartAt(); result = toLocalDateTime("2001-01-02 10:00:00");
        }};
        try {
            service.importData(batch);
            Assert.fail("Expected exceptin here!");
        }catch (BookingException e){
            Assert.assertEquals(101, e.getErrorInfo().getErrorCode());
        }

        new Verifications(){{
            sync.lock(Constants.IMPORT_BOOKING_BATCH); times = 1;
            booking.hasIntersection(anyString,(LocalDateTime)any,(LocalDateTime)any); times = 0;
            booking.insert((BookingEntity) any); times = 0;
        }};
    }

    @Test
    public void tesHasIntersection(@Mocked final BookingBatchRequest batch, @Mocked BookingBatchRequest.BookingRecord record) {
        new Expectations(){{
            batch.getBeginsAt();result = toLocalTime("09:00");
            batch.getFinishesAt();result = toLocalTime("18:00");
            batch.getRecords();result = Collections.singletonList(record);
            record.getDuration(); result = 3;
            record.getEmployee(); result = "Alex";
            record.getStartAt(); result = toLocalDateTime("2001-01-02 10:00:00");
            booking.hasIntersection("Alex",toLocalDateTime("2001-01-02 10:00:00"),toLocalDateTime("2001-01-02 13:00:00")); result = true;
        }};
        try {
            service.importData(batch);
            Assert.fail("Expected exceptin here!");
        }catch (BookingException e){
            Assert.assertEquals(102, e.getErrorInfo().getErrorCode());
        }

        new Verifications(){{
            sync.lock(Constants.IMPORT_BOOKING_BATCH); times = 1;
            booking.hasIntersection(anyString,(LocalDateTime)any,(LocalDateTime)any); times = 1;
            booking.insert((BookingEntity) any); times = 0;
        }};
    }

    @Test
    public void testDuplicateTimestamp(@Mocked final BookingBatchRequest batch, @Mocked BookingBatchRequest.BookingRecord record) {
        new Expectations(){{
            batch.getBeginsAt();result = toLocalTime("09:00");
            batch.getFinishesAt();result = toLocalTime("18:00");
            batch.getRecords();result = Collections.singletonList(record);
            record.getBookingTimestamp(); result = toLocalDateTime("2001-01-01 10:10:10");
            record.getDuration(); result = 3;
            record.getEmployee(); result = "Alex";
            record.getStartAt(); result = toLocalDateTime("2001-01-02 10:00:00");
            booking.hasIntersection("Alex",toLocalDateTime("2001-01-02 10:00:00"),toLocalDateTime("2001-01-02 13:00:00")); result = false;
            booking.insert((BookingEntity) any); result = new DuplicateKeyException("Test");

        }};
        try {
            service.importData(batch);
            Assert.fail("Expected exceptin here!");
        }catch (BookingException e){
            Assert.assertEquals(103, e.getErrorInfo().getErrorCode());
        }

        new Verifications(){{
            sync.lock(Constants.IMPORT_BOOKING_BATCH); times = 1;
            booking.hasIntersection(anyString,(LocalDateTime)any,(LocalDateTime)any); times = 1;
            booking.insert((BookingEntity) any); times = 1;
        }};
    }

    @Test
    public void testImportData(@Mocked final BookingBatchRequest batch,
                               @Mocked BookingBatchRequest.BookingRecord record1,
                               @Mocked BookingBatchRequest.BookingRecord record2,
                               @Mocked BookingBatchRequest.BookingRecord record3) throws BookingException {
        new Expectations(){{
            batch.getBeginsAt();result = toLocalTime("09:00");
            batch.getFinishesAt();result = toLocalTime("18:00");
            batch.getRecords();result = Arrays.asList(record1,record2,record3);

            record1.getBookingTimestamp(); result = toLocalDateTime("2001-01-01 10:10:10");
            record1.getDuration(); result = 3;
            record1.getEmployee(); result = "Alex";
            record1.getStartAt(); result = toLocalDateTime("2001-01-02 10:00:00");
            booking.hasIntersection(anyString,(LocalDateTime)any,(LocalDateTime)any); result = false; times = 3;

            record2.getBookingTimestamp(); result = toLocalDateTime("2001-01-01 12:12:12");
            record2.getDuration(); result = 3;
            record2.getEmployee(); result = "Alex";
            record2.getStartAt(); result = toLocalDateTime("2001-01-03 13:00:00");

            record3.getBookingTimestamp(); result = toLocalDateTime("2001-01-01 15:00:00");
            record3.getDuration(); result = 3;
            record3.getEmployee(); result = "Peter";
            record3.getStartAt(); result = toLocalDateTime("2001-01-02 10:00:00");
        }};

        final ScheduleTableResponse scheduleTable = service.importData(batch);
        Assert.assertEquals(2,scheduleTable.getTable().size());
        Assert.assertEquals(2,scheduleTable.getTable().get(toLocalDate("2001-01-02")).size());
        Assert.assertEquals(1,scheduleTable.getTable().get(toLocalDate("2001-01-03")).size());

        new Verifications(){{
            sync.lock(Constants.IMPORT_BOOKING_BATCH); times = 1;
            List<BookingEntity> entities = new ArrayList<>();
            booking.insert(withCapture(entities)); times = 3;
            assertEntity(entities.get(0),"Alex", toLocalDateTime("2001-01-01 10:10:10"), toLocalDateTime("2001-01-02 10:00:00"), toLocalDateTime("2001-01-02 13:00:00"));
            assertEntity(entities.get(1),"Alex", toLocalDateTime("2001-01-01 12:12:12"), toLocalDateTime("2001-01-03 13:00:00"), toLocalDateTime("2001-01-03 16:00:00"));
            assertEntity(entities.get(2),"Peter", toLocalDateTime("2001-01-01 15:00:00"), toLocalDateTime("2001-01-02 10:00:00"), toLocalDateTime("2001-01-02 13:00:00"));
        }};
    }

    @Test
    public void testGetSchedule(@Mocked BookingEntity entit1,
                                @Mocked BookingEntity entity2,
                                @Mocked BookingEntity entity3) throws BookingException {
        new Expectations() {{
            entit1.getEmployee(); result = "Alex";
            entit1.getBeginsAt(); result = toLocalDateTime("2001-01-02 10:00:00");
            entit1.getFinishesAt(); result = toLocalDateTime("2001-01-02 13:00:00");

            entity2.getEmployee(); result = "Alex";
            entity2.getBeginsAt(); result = toLocalDateTime("2001-01-03 13:15:00");
            entity2.getFinishesAt(); result = toLocalDateTime("2001-01-03 18:45:00");

            entity3.getEmployee(); result = "Peter";
            entity3.getBeginsAt(); result = toLocalDateTime("2001-01-02 10:00:00");
            entity3.getFinishesAt(); result = toLocalDateTime("2001-01-02 13:00:00");

            booking.findBy(withNull(), withNull(),withNull()); result = Arrays.asList(entit1,entity3,entity2);
        }};

        final ScheduleTableResponse scheduleTable = service.getSchedule(null,null,null);
        Assert.assertEquals(2,scheduleTable.getTable().size());

        final List<ScheduleTableResponse.ScheduleRecord> r_20010102 = scheduleTable.getTable().get(toLocalDate("2001-01-02"));
        final List<ScheduleTableResponse.ScheduleRecord> r_20010103 = scheduleTable.getTable().get(toLocalDate("2001-01-03"));

        Assert.assertEquals(2, r_20010102.size());
        Assert.assertEquals(1, r_20010103.size());

        Assert.assertEquals("Alex", r_20010102.get(0).getEmployee());
        Assert.assertEquals(toLocalTime("10:00"), r_20010102.get(0).getBeginsAt());
        Assert.assertEquals(toLocalTime("13:00"), r_20010102.get(0).getFinishesAt());
        Assert.assertEquals("Peter", r_20010102.get(1).getEmployee());
        Assert.assertEquals(toLocalTime("10:00"), r_20010102.get(1).getBeginsAt());
        Assert.assertEquals(toLocalTime("13:00"), r_20010102.get(1).getFinishesAt());
        Assert.assertEquals("Alex", r_20010103.get(0).getEmployee());
        Assert.assertEquals(toLocalTime("13:15"), r_20010103.get(0).getBeginsAt());
        Assert.assertEquals(toLocalTime("18:45"), r_20010103.get(0).getFinishesAt());
    }

    private void assertEntity(final BookingEntity entity,final String employee,final LocalDateTime ts, final LocalDateTime beginsAt,final LocalDateTime finishesAt){
        Assert.assertEquals(employee,entity.getEmployee());
        Assert.assertEquals(ts,entity.getTs());
        Assert.assertEquals(beginsAt,entity.getBeginsAt());
        Assert.assertEquals(finishesAt,entity.getFinishesAt());
    }
}
