package by.inventain.trial.bookingbatch;

/**
 * @author alexander.guzanov
 */
public class BookingException extends Exception {
    private ErrorInfo errorInfo;

    public BookingException(ErrorInfo errorInfo) {
        super(errorInfo.getDescription());
        this.errorInfo = errorInfo;
    }

    public ErrorInfo getErrorInfo() {
        return errorInfo;
    }
}
