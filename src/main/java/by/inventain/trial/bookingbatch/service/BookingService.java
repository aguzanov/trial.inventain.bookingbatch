package by.inventain.trial.bookingbatch.service;

import by.inventain.trial.bookingbatch.BookingException;
import by.inventain.trial.bookingbatch.Constants;
import by.inventain.trial.bookingbatch.ErrorInfo;
import by.inventain.trial.bookingbatch.controller.BookingBatchRequest;
import by.inventain.trial.bookingbatch.controller.BookingBatchRequest.BookingRecord;
import by.inventain.trial.bookingbatch.controller.ScheduleTableResponse;
import by.inventain.trial.bookingbatch.dao.BookingDao;
import by.inventain.trial.bookingbatch.dao.BookingEntity;
import by.inventain.trial.bookingbatch.dao.SyncDao;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DuplicateKeyException;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

import javax.validation.constraints.Null;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.List;

/**
 * Service business login implementation
 *
 * @author alexander.guzanov
 */
@Component
public class BookingService {
    private static final Logger logger = LoggerFactory.getLogger(BookingService.class);
    @Autowired
    private BookingDao booking;
    @Autowired
    private SyncDao sync;

    /**
     * Import data and returns schedule table for imported data.
     * <p>
     * This method is invoked under common transaction scope. If something goes wrong, all  new data will be rolled back from databse
     *
     * @param batch request
     * @return
     * @throws BookingException
     */
    @Transactional(rollbackFor = BookingException.class, propagation = Propagation.REQUIRES_NEW)
    public ScheduleTableResponse importData(final BookingBatchRequest batch) throws BookingException {
        logger.trace("Processing batch. Working time: [{},{}], record count={}", batch.getBeginsAt(), batch.getFinishesAt(), batch.getRecords().size());
        // only one process can be inside this critical section
        sync.lock(Constants.IMPORT_BOOKING_BATCH);

        final List<BookingRecord> records = batch.getRecords();
        final List<BookingEntity> items = new ArrayList<>(records.size());
        for (final BookingRecord bookingRecord : records) {
            logger.trace("Processing record {}", bookingRecord);
            final LocalDateTime finishesAt = bookingRecord.getStartAt().plusHours(bookingRecord.getDuration());
            if (finishesAt.toLocalTime().compareTo(batch.getFinishesAt()) > 0 || bookingRecord.getStartAt().toLocalTime().compareTo(batch.getBeginsAt()) < 0) {
                logger.trace("Wrong booking time for record {}", bookingRecord);
                throw new BookingException(ErrorInfo.bookingTimeBeyondWorkingHours(bookingRecord.getStartAt().toLocalTime(), finishesAt.toLocalTime(),
                        batch.getBeginsAt(), batch.getFinishesAt()));
            }
            if (booking.hasIntersection(bookingRecord.getEmployee(), bookingRecord.getStartAt(), finishesAt)) {
                logger.trace("Record {} has intersection", bookingRecord);
                throw new BookingException(ErrorInfo.hasIntersection(bookingRecord.getEmployee(), bookingRecord.getStartAt(), finishesAt));
            }

            final BookingEntity entity = new BookingEntity(bookingRecord.getBookingTimestamp(), bookingRecord.getStartAt(), finishesAt, bookingRecord.getEmployee());
            try {
                booking.insert(entity);
            } catch (DuplicateKeyException e) {
                throw new BookingException(ErrorInfo.duplicateSchedulingTimestamp(bookingRecord.getBookingTimestamp()));
            }
            items.add(entity);
        }

        Collections.sort(items, this::compare);

        return buildSchedule(items);
    }

    /**
     * Get scheduling information
     *
     * @param employee emloyee
     * @param from     range begining
     * @param to       range finishing
     * @return
     */
    @Transactional(propagation = Propagation.REQUIRED, readOnly = true)
    public ScheduleTableResponse getSchedule(@Null String employee, @Null LocalDate from, @Null LocalDate to) {
        logger.trace("Get schedule table for employee={}, [{} - {}]", employee, from, to);
        return buildSchedule(booking.findBy(employee, from, to));
    }

    private ScheduleTableResponse buildSchedule(final Collection<BookingEntity> items) {
        final ScheduleTableResponse result = new ScheduleTableResponse();
        for (BookingEntity item : items) {
            result.addRecord(item);
        }
        return result;
    }

    private int compare(final BookingEntity o1, final BookingEntity o2) {
        int result = o1.getBeginsAt().compareTo(o2.getBeginsAt());
        if (result == 0) {
            result = o1.getFinishesAt().compareTo(o2.getFinishesAt());
        }

        if (result == 0) {
            result = o1.getEmployee().compareTo(o2.getEmployee());
        }
        return result;
    }
}
