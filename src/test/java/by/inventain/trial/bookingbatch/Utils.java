package by.inventain.trial.bookingbatch;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;

/**
 * @author alexander.guzanov
 */
public class Utils {

    public static LocalTime toLocalTime(final String value) {
        return LocalTime.parse(value, Constants.TIME_FORMATTER);
    }

    public static LocalDateTime toLocalDateTime(final String value) {
        return LocalDateTime.parse(value, Constants.DATE_TIME_FORMATTER);
    }

    public static LocalDate toLocalDate(final String value) {
        return LocalDate.parse(value, Constants.DATE_FORMATTER);
    }
}
