package by.inventain.trial.bookingbatch;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
@EnableAutoConfiguration
public class BookingbatchApplication {

    public static void main(String[] args) {
        SpringApplication.run(BookingbatchApplication.class, args);
    }
}
