package by.inventain.trial.bookingbatch.dao;

/**
 * Object mapping for lock entity
 *
 * @author alexander.guzanov
 */
public class LockEntity {
    private final String lockName;

    public LockEntity(String lockName) {
        this.lockName = lockName;
    }

    public String getLockName() {
        return lockName;
    }
}
