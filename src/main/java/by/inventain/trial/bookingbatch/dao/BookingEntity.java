package by.inventain.trial.bookingbatch.dao;

import java.time.LocalDateTime;

/**
 * Object mapping to database record
 *
 * @author alexander.guzanov
 */
public class BookingEntity {
    /**
     * Booking timestamp, primary key
     */
    private final LocalDateTime ts;
    /**
     * starting of meeting
     */
    private final LocalDateTime beginsAt;
    /**
     * finishing of meeting
     */
    private final LocalDateTime finishesAt;
    /**
     * employee's name
     */
    private final String employee;

    public BookingEntity(final LocalDateTime ts, final LocalDateTime beginsAt, final LocalDateTime finishesAt, final String employee) {
        this.ts = ts;
        this.beginsAt = beginsAt;
        this.finishesAt = finishesAt;
        this.employee = employee;
    }

    public LocalDateTime getTs() {
        return ts;
    }

    public LocalDateTime getBeginsAt() {
        return beginsAt;
    }

    public LocalDateTime getFinishesAt() {
        return finishesAt;
    }

    public String getEmployee() {
        return employee;
    }
}
