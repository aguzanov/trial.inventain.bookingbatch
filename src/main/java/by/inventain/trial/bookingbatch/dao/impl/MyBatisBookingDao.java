package by.inventain.trial.bookingbatch.dao.impl;

import by.inventain.trial.bookingbatch.dao.BookingDao;
import by.inventain.trial.bookingbatch.dao.BookingEntity;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.Collection;

/**
 * MyBatis {@link BookingDao} implementation
 *
 * @author alexander.guzanov
 */

@Mapper
public interface MyBatisBookingDao extends BookingDao {

    @Override
    void insert(BookingEntity entity);

    @Override
    boolean hasIntersection(@Param("employee") String employee, @Param("from") LocalDateTime from, @Param("to") LocalDateTime to);

    @Override
    Collection<BookingEntity> findBy(@Param("employee") String employee, @Param("from") LocalDate from, @Param("to") LocalDate to);
}
