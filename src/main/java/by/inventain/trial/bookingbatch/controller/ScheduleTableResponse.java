package by.inventain.trial.bookingbatch.controller;

import by.inventain.trial.bookingbatch.dao.BookingEntity;
import com.fasterxml.jackson.annotation.JsonProperty;

import java.time.LocalDate;
import java.time.LocalTime;
import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;

/**
 * Schedule table.
 * </br>
 * It used as response in RESful service.
 *
 * @author alexander.guzanov
 */
public class ScheduleTableResponse {
    /**
     * Schedule records by day
     */
    @JsonProperty
    private LinkedHashMap<LocalDate, List<ScheduleRecord>> table = new LinkedHashMap<>();

    /**
     * Add record to table
     *
     * @param entity booking entity which contains scheduling information
     */
    public void addRecord(BookingEntity entity) {
        final LocalDate day = entity.getBeginsAt().toLocalDate();
        List<ScheduleRecord> records = table.get(day);
        if (records == null) {
            records = new ArrayList<>();
            table.put(day, records);
        }

        records.add(new ScheduleRecord(entity.getEmployee(), entity.getBeginsAt().toLocalTime(), entity.getFinishesAt().toLocalTime()));
    }

    public LinkedHashMap<LocalDate, List<ScheduleRecord>> getTable() {
        return table;
    }

    /**
     * Schedule record
     */
    public static class ScheduleRecord {
        /**
         * Employee
         */
        @JsonProperty
        private String employee;
        /**
         * Time of meeting starting
         */
        @JsonProperty
        private LocalTime beginsAt;
        /**
         * Time of meeting finishing
         */
        @JsonProperty
        private LocalTime finishesAt;

        public ScheduleRecord(String employee, LocalTime beginsAt, LocalTime finishesAt) {
            this.employee = employee;
            this.beginsAt = beginsAt;
            this.finishesAt = finishesAt;
        }

        public LocalTime getBeginsAt() {
            return beginsAt;
        }

        public LocalTime getFinishesAt() {
            return finishesAt;
        }

        public String getEmployee() {
            return employee;
        }
    }
}
