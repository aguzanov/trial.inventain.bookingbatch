package by.inventain.trial.bookingbatch.dao;

import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;

/**
 * This dao helps to synchronize imporing data
 *
 * @author alexander.guzanov
 */
public interface SyncDao {

    /**
     * Makes pessimistic lock
     *
     * @param lockName lock name
     * @return
     */
    @Transactional(propagation = Propagation.MANDATORY)
    LockEntity lock(String lockName);
}
