package by.inventain.trial.bookingbatch;

import java.time.LocalDateTime;
import java.time.LocalTime;
import java.util.Map;
import java.util.stream.Collectors;
import java.util.stream.Stream;


/**
 * Error description with detaails
 *
 * @author alexander.guzanov
 */
public class ErrorInfo {
    /**
     * error code
     */
    private int errorCode;
    /**
     * description
     */
    private String description;
    /**
     * details
     */
    private Map<String, Object> details = null;

    ErrorInfo(int errorCode, String description, Object[]... params) {
        this.errorCode = errorCode;
        this.description = description;
        this.details = Stream.of(params).collect(Collectors.toMap(x -> x[0].toString(), x -> x[1]));
    }

    private static Object[] pair(String name, Object value) {
        return new Object[]{name, value};
    }

    /**
     * Booking time is wrong, because it's beyond working day time range
     *
     * @param bookingBeginsAt      begining of meeting
     * @param bookingFinishesAt    finishing of meeting
     * @param workingDayBeginsAt   begining of working day
     * @param workingDayFinishesAt finishing of working day
     * @return
     */
    public static ErrorInfo bookingTimeBeyondWorkingHours(final LocalTime bookingBeginsAt, final LocalTime bookingFinishesAt,
                                                          final LocalTime workingDayBeginsAt, final LocalTime workingDayFinishesAt) {
        return new ErrorInfo(101, "Booking time is beyond working hours",
                pair("bookingBeginsAt", bookingBeginsAt.format(Constants.TIME_FORMATTER)),
                pair("bookingFinishesAt", bookingFinishesAt.format(Constants.TIME_FORMATTER)),
                pair("workingDayBeginsAt", workingDayBeginsAt.format(Constants.TIME_FORMATTER)),
                pair("workingDayFinishesAt", workingDayFinishesAt.format(Constants.TIME_FORMATTER)));
    }

    /**
     * New scheduling record intersects with existings
     *
     * @param employee          employee name
     * @param bookingBeginsAt   beginiing of meeting
     * @param bookingFinishesAt finishing of meeting
     * @return
     */
    public static ErrorInfo hasIntersection(final String employee, final LocalDateTime bookingBeginsAt, final LocalDateTime bookingFinishesAt) {
        return new ErrorInfo(102, "Booking time intersects with another time",
                pair("employee", employee),
                pair("bookingBeginsAt", bookingBeginsAt.format(Constants.DATE_TIME_FORMATTER)),
                pair("bookingFinishesAt", bookingFinishesAt.format(Constants.DATE_TIME_FORMATTER)));
    }

    /**
     * Duplicate scheduling timestamp
     *
     * @param ts timestamp
     * @return
     */
    public static ErrorInfo duplicateSchedulingTimestamp(final LocalDateTime ts) {
        return new ErrorInfo(103, "Bookings timestamp has duplicates",
                pair("ts", ts.format(Constants.DATE_TIME_FORMATTER)));
    }

    /**
     * Wrong REST invocation
     *
     * @param url   method url
     * @param field wrong field, optional
     * @param value wrong field value, optional
     * @return
     */
    public static ErrorInfo wrongRequest(final String url, final String field, final Object value) {
        return new ErrorInfo(104, "Wrong request",
                pair("url", url),
                pair("field", field),
                pair("value", value));
    }

    /**
     * Common error.
     *
     * @param url method url
     * @return
     */
    public static ErrorInfo error(final String url) {
        return new ErrorInfo(100, "Internal server error",
                pair("url", url));
    }

    public int getErrorCode() {
        return errorCode;
    }

    public String getDescription() {
        return description;
    }

    public Map<String, Object> getDetails() {
        return details;
    }

}


