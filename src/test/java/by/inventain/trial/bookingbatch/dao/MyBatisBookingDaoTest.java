package by.inventain.trial.bookingbatch.dao;


import by.inventain.trial.bookingbatch.dao.impl.MyBatisBookingDao;
import com.github.springtestdbunit.DbUnitTestExecutionListener;
import com.github.springtestdbunit.annotation.DatabaseSetup;
import com.github.springtestdbunit.annotation.ExpectedDatabase;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.TestExecutionListeners;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.context.support.DependencyInjectionTestExecutionListener;

import java.util.ArrayList;
import java.util.List;

import static by.inventain.trial.bookingbatch.Utils.toLocalDate;
import static by.inventain.trial.bookingbatch.Utils.toLocalDateTime;
import static org.junit.Assert.*;

/**
 * @author alexander.guzanov
 */
@RunWith(SpringRunner.class)
@SpringBootTest
@TestExecutionListeners({DependencyInjectionTestExecutionListener.class, DbUnitTestExecutionListener.class})
public class MyBatisBookingDaoTest {
    @Autowired
    private MyBatisBookingDao booking;

    @Test
    @DatabaseSetup("booking.testInsert.setup.xml")
    @ExpectedDatabase(value = "booking.testInsert.expected.xml", table = "BOOKING")
    public void testInsert() {
        booking.insert(new BookingEntity(toLocalDateTime("2016-08-08 13:12:00"), toLocalDateTime("2016-08-18 16:15:00"), toLocalDateTime("2016-08-18 18:15:00"), "ALEX"));
    }

    @Test(expected = org.springframework.dao.DuplicateKeyException.class)
    @DatabaseSetup(value = "booking.testInsert.expected.xml")
    @ExpectedDatabase(value = "booking.testInsert.expected.xml", table = "BOOKING")
    public void testInsert_failed_DuplicateException() {
        testInsert();
    }

    @Test
    @DatabaseSetup(value = "booking.testHasIntersection.setup.xml")
    public void testHasIntersection_1() {
        assertTrue(booking.hasIntersection("ALEX",toLocalDateTime("2016-08-18 15:15:00"), toLocalDateTime("2016-08-18 19:15:00")));
    }

    @Test
    @DatabaseSetup(value = "booking.testHasIntersection.setup.xml")
    public void testHasIntersection_2() {
        assertTrue(booking.hasIntersection("ALEX",toLocalDateTime("2016-08-18 17:15:00"), toLocalDateTime("2016-08-18 19:15:00")));
    }

    @Test
    @DatabaseSetup(value = "booking.testHasIntersection.setup.xml")
    public void testHasIntersection_3() {
        assertTrue(booking.hasIntersection("ALEX",toLocalDateTime("2016-08-18 17:15:00"), toLocalDateTime("2016-08-18 17:35:00")));
    }

    @Test
    @DatabaseSetup(value = "booking.testHasIntersection.setup.xml")
    public void testHasIntersection_4() {
        assertTrue(booking.hasIntersection("ALEX",toLocalDateTime("2016-08-18 15:15:00"), toLocalDateTime("2016-08-18 17:35:00")));
    }

    @Test
    @DatabaseSetup(value = "booking.testHasIntersection.setup.xml")
    public void testHasIntersection_5() {
        assertFalse(booking.hasIntersection("ALEX1",toLocalDateTime("2016-08-18 15:15:00"), toLocalDateTime("2016-08-18 17:35:00")));
    }

    @Test
    @DatabaseSetup(value = "booking.testFindBy.setup.xml")
    public void testFindByEmployee() {
        final List<BookingEntity> items = new ArrayList<>(booking.findBy("ALEX", null, null));
        assertEquals(items.size(),2);
        assertEquals(items.get(0).getTs(),toLocalDateTime("2016-08-08 14:15:00"));
        assertEquals(items.get(1).getTs(),toLocalDateTime("2016-08-08 13:12:00"));
    }

    @Test
    @DatabaseSetup(value = "booking.testFindBy.setup.xml")
    public void testFindByEmployeeAndFrom() {
        final List<BookingEntity> items = new ArrayList<>(booking.findBy("ALEX", toLocalDate("2016-08-17"), null));
        assertEquals(items.size(),1);
        assertEquals(items.get(0).getTs(),toLocalDateTime("2016-08-08 13:12:00"));
    }

    @Test
    @DatabaseSetup(value = "booking.testFindBy.setup.xml")
    public void testFindByEmployeeAndTo() {
        final List<BookingEntity> items = new ArrayList<>(booking.findBy("ALEX", null, toLocalDate("2016-08-17")));
        assertEquals(items.size(),1);
        assertEquals(items.get(0).getTs(),toLocalDateTime("2016-08-08 14:15:00"));
    }


    @Test
    @DatabaseSetup(value = "booking.testFindBy.setup.xml")
    public void testFindByEmployeeAndFromAndTo() {
        final List<BookingEntity> items = new ArrayList<>(booking.findBy("ALEX", toLocalDate("2016-08-09"), toLocalDate("2016-08-19")));
        assertEquals(items.size(),2);
        assertEquals(items.get(0).getTs(),toLocalDateTime("2016-08-08 14:15:00"));
        assertEquals(items.get(1).getTs(),toLocalDateTime("2016-08-08 13:12:00"));
    }

    @Test
    @DatabaseSetup(value = "booking.testFindBy.setup.xml")
    public void testFindBy() {
        final List<BookingEntity> items = new ArrayList<>(booking.findBy(null, null, null));
        assertEquals(items.size(),3);
        assertEquals(items.get(0).getTs(),toLocalDateTime("2016-08-09 17:05:00"));
        assertEquals(items.get(1).getTs(),toLocalDateTime("2016-08-08 14:15:00"));
        assertEquals(items.get(2).getTs(),toLocalDateTime("2016-08-08 13:12:00"));
    }

    @Test
    @DatabaseSetup(value = "booking.testFindBy.setup.xml")
    public void testFindBy_notBound() {
        final List<BookingEntity> items = new ArrayList<>(booking.findBy("ALEX1", null, null));
        assertTrue(items.isEmpty());
    }
}
