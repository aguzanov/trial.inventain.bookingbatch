package by.inventain.trial.bookingbatch.controller;

import by.inventain.trial.bookingbatch.BookingException;
import by.inventain.trial.bookingbatch.ErrorInfo;
import by.inventain.trial.bookingbatch.dao.BookingEntity;
import by.inventain.trial.bookingbatch.service.BookingService;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;

import java.nio.file.Files;
import java.nio.file.Paths;

import static by.inventain.trial.bookingbatch.Utils.toLocalDateTime;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

/**
 * @author alexander.guzanov
 */

@RunWith(SpringRunner.class)
@SpringBootTest
@AutoConfigureMockMvc
@DirtiesContext(classMode = DirtiesContext.ClassMode.AFTER_EACH_TEST_METHOD)
public class BookingControllerTest {

    @Autowired
    private MockMvc mvc;

    @MockBean
    private BookingService bookingService;


    @Test
    public void testImportWrongRequest() throws Exception {
        this.mvc.perform(post("/booking/import").contentType(MediaType.APPLICATION_JSON).content(getResource("importData.wrongFormat.json"))).andExpect(status().is(400)).andExpect(content()
                .json(getResource("error.testImportWrongRequest.json")));
    }

    @Test
    public void testGetScheduleWrongRequest() throws Exception {
        this.mvc.perform(get("/booking/schedule").contentType(MediaType.APPLICATION_JSON).param("from", "WRONGVALUE")).andExpect(status().is(400)).andExpect(content()
                .json(getResource("error.testGetScheduleWrongRequest.json")));
    }

    @Test
    public void testGetSchedule() throws Exception {
        final ScheduleTableResponse scheduleTable = new ScheduleTableResponse();
        scheduleTable.addRecord(new BookingEntity(toLocalDateTime("2001-01-01 01:01:01"), toLocalDateTime("2001-01-02 11:00:01"), toLocalDateTime("2001-01-02 10:00:00"), "EMP001"));
        scheduleTable.addRecord(new BookingEntity(toLocalDateTime("2001-01-01 01:02:01"), toLocalDateTime("2001-01-02 11:00:01"), toLocalDateTime("2001-01-02 10:00:00"), "EMP002"));
        scheduleTable.addRecord(new BookingEntity(toLocalDateTime("2001-01-01 01:03:01"), toLocalDateTime("2001-01-03 12:00:01"), toLocalDateTime("2001-01-03 13:15:00"), "EMP003"));
        Mockito.when(bookingService.getSchedule(Mockito.anyString(), Mockito.any(), Mockito.any())).thenReturn(scheduleTable);

        this.mvc.perform(get("/booking/schedule").contentType(MediaType.APPLICATION_JSON)).andExpect(status().is(200)).andExpect(content()
                .json(getResource("schedule.json")));
    }

    @Test
    public void testGetScheduleByEmployee() throws Exception {
        final ScheduleTableResponse scheduleTable = new ScheduleTableResponse();
        scheduleTable.addRecord(new BookingEntity(toLocalDateTime("2001-01-01 01:01:01"), toLocalDateTime("2001-01-02 11:00:01"), toLocalDateTime("2001-01-02 10:00:00"), "EMP001"));
        scheduleTable.addRecord(new BookingEntity(toLocalDateTime("2001-01-01 01:02:01"), toLocalDateTime("2001-01-02 11:00:01"), toLocalDateTime("2001-01-02 10:00:00"), "EMP001"));
        scheduleTable.addRecord(new BookingEntity(toLocalDateTime("2001-01-01 01:03:01"), toLocalDateTime("2001-01-03 12:00:01"), toLocalDateTime("2001-01-03 13:15:00"), "EMP001"));
        Mockito.when(bookingService.getSchedule(Mockito.anyString(), Mockito.any(), Mockito.any())).thenReturn(scheduleTable);

        this.mvc.perform(get("/booking/schedule/EMP001").contentType(MediaType.APPLICATION_JSON)).andExpect(status().is(200)).andExpect(content()
                .json(getResource("schedule.byEmployee.json")));
    }

    @Test
    public void testImportData() throws Exception {
        final ScheduleTableResponse scheduleTable = new ScheduleTableResponse();
        scheduleTable.addRecord(new BookingEntity(toLocalDateTime("2001-01-01 01:01:01"), toLocalDateTime("2001-01-02 11:00:01"), toLocalDateTime("2001-01-02 10:00:00"), "EMP001"));
        scheduleTable.addRecord(new BookingEntity(toLocalDateTime("2001-01-01 01:02:01"), toLocalDateTime("2001-01-02 11:00:01"), toLocalDateTime("2001-01-02 10:00:00"), "EMP002"));
        scheduleTable.addRecord(new BookingEntity(toLocalDateTime("2001-01-01 01:03:01"), toLocalDateTime("2001-01-03 12:00:01"), toLocalDateTime("2001-01-03 13:15:00"), "EMP003"));
        Mockito.when(bookingService.importData(Mockito.any())).thenReturn(scheduleTable);


        this.mvc.perform(post("/booking/import").contentType(MediaType.APPLICATION_JSON).content(getResource("importData.json"))).andExpect(status().is(200)).andExpect(content()
                .json(getResource("schedule.json")));
    }

    @Test
    public void testImportDataBookingException() throws Exception {
        Mockito.when(bookingService.importData(Mockito.any())).thenThrow(new BookingException(ErrorInfo.duplicateSchedulingTimestamp(toLocalDateTime("2001-01-01 01:03:01"))));

        this.mvc.perform(post("/booking/import").contentType(MediaType.APPLICATION_JSON).content(getResource("importData.json"))).andExpect(status().is(400)).andExpect(content()
                .json(getResource("error.testImportDataBookingException.json")));
    }

    @Test
    public void testImportDataException() throws Exception {
        Mockito.when(bookingService.importData(Mockito.any())).thenThrow(new RuntimeException());

        this.mvc.perform(post("/booking/import").contentType(MediaType.APPLICATION_JSON).content(getResource("importData.json"))).andExpect(status().is(400)).andExpect(content()
                .json(getResource("error.testImportException.json")));
    }

    private String getResource(final String name) {
        try {
            return new String(Files.readAllBytes(Paths.get(getClass().getResource(name).toURI())));
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }
}
